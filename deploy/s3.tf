# together with this block, we should add env var
# ("S3_STORAGE_BUCKET_NAME", "S3_STORAGE_BUCKET_REGION") 
# to ecs/container-definintions.json.tpl
resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files"
  acl           = "public-read"
  force_destroy = true
}