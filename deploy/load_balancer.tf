# There two types of load balancers: 1. Application lb 2. Network lb
# Application lb handles requests at HTTP level (http & https)
# Network lb handles requests at network level (tcp & udp)
resource "aws_lb" "api" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application"
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id # set public to be accessible from Internet
  ]

  security_groups = [aws_security_group.lb.id]

  tags = local.common_tags
}

# Target group is the group of servers that load balancer can forward request to.
resource "aws_lb_target_group" "api" {
  name        = "${local.prefix}-api"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  port        = 8000 #Port that proxy will run on in ECS task.

  health_check {
    path = "/admin/login/"
  }
}

# Listener accepts the request to lb, as an entry point of lb
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}

resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  } #Allow all connections from public internet into lb

  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  } #Access out from lb into app

  tags = local.common_tags
}